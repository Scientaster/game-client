import {Loader} from 'pixi.js'
import {middleware as tmxMapMiddleware} from './tmx/TmxMap';

const spritesheets = ['dwarf_fixed'];

export default {
  loadAllAssets(cb) {
    Loader.shared
      .add(
        [
          ...spritesheets.map((asset) => 'assets/' + asset + '.json'),
        ])
      .add('BaseMap', 'levels/base.tmx')
      .use(tmxMapMiddleware)
      .load(cb);
  },
};
