import {Container, Loader} from 'pixi.js';
import tmx from 'tmx-parser';
import TmxTileset from './TmxTileset';
import TmxLayer from './TmxLayer';
import Coordinates from '../game/Coordinates';

class TmxMap extends Container {
  constructor(resourceId) {
    super();

    const resource = Loader.shared.resources[resourceId];

    this.tileSets = resource.data.tileSets.map((tileData) => {
      return new TmxTileset(tileData);
    });
    this.layers = resource.data.layers
      .filter(layerData => layerData.type === 'tile')
      .map((layerData) => {
        const tileLayer = new TmxLayer(layerData, this.tileSets);
        return tileLayer;
      });

    this.layers.forEach((layer) => this.addChild(layer));
  }

  act() {
    const [x, y] = Coordinates.gameToScreen(0, 0);
    this.x = x;
    this.y = y;
  }

}

export const middleware = (resource, next) => {
  if (resource.extension !== 'tmx') return next();

    const xmlString = resource.xhr.responseText;
    const pathToFile = resource.url;

    tmx.parse(xmlString, pathToFile, (error, map) => {
      if (error) throw error;

      resource.data = map;
      next();
    });
}


export default TmxMap;