import {Texture, Rectangle} from 'pixi.js';

export default class TileSet {
  constructor (tileSet) {
    this.textures = [];
    this.baseTexture = Texture.from('assets/' + tileSet.image.source)
    this.firstGid = tileSet.firstGid;
    this.setTileTextures(tileSet)
  }

  setTileTextures (tileSet) {
    for (let y = tileSet.margin; y < tileSet.image.height; y += tileSet.tileHeight + tileSet.spacing) {
      for (let x = tileSet.margin; x < tileSet.image.width; x += tileSet.tileWidth + tileSet.spacing) {
        const tileRectangle = new Rectangle(x, y, tileSet.tileWidth, tileSet.tileHeight)
        this.textures.push(new Texture(this.baseTexture, tileRectangle))
      }
    }
  }
}