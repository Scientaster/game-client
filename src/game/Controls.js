let controlListeners = {};

module.exports = {
  registerControls() {
    document.onkeydown = function(event) {
      if (event.code in controlListeners) {
        controlListeners[event.code].map(listener => listener.down(event));
      }
    };
    
    document.onkeyup = function(event) {
      if (event.code in controlListeners) {
        controlListeners[event.code].map(listener => listener.up(event));
      }
    };
  },

  registerObserver(keyCode, listener) {
    if (!controlListeners[keyCode]) {
      controlListeners[keyCode] = [];
    } 

    controlListeners[keyCode].push(listener);
  },

  deregisterObserver(keyCode, listener) {
    if (controlListeners[keyCode]) {
      controlListeners[keyCode] = controlListeners[keyCode].filter(registeredListener => listener !== registeredListener);
    }
  }
}