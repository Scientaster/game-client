let instance = null;
let player = null;

export default {
  setInstance(app) {
    instance = app;
  },
  getInstance() {
    return instance;
  },
  setPlayer(pl) {
    player = pl;
  },
  getPlayer() {
    return player;
  }
};
