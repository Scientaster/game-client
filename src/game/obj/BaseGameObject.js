import AppSingleton from '../AppSingleton';
import { AnimatedSprite, utils } from 'pixi.js';
import Coordinates from '../Coordinates';

export default class BaseGameObject {
  constructor(animations, currentAnimation, scale, x, y) {
    this.x = x;
    this.y = y;
    this.currentAnimation = currentAnimation;
    this.animations = {};
    this.mirrorX = false;

    for (const [animationName, animationAttributes] of Object.entries(animations)) {
      const textures = [];
      for (let i = 0; i < animationAttributes.count; i++) {
        textures.push({
          texture: utils.TextureCache[animationAttributes.frameKey.replace('__FRAME__', i)],
          time: animationAttributes.speed,
        });
      }
      this.animations[animationName] = new AnimatedSprite(textures);
      this.animations[animationName].scale.set(scale);
    }

    this.changeAnimation(currentAnimation);
  }

  addToStage(stage) {
    for (const [animationName, animatedSprite] of Object.entries(this.animations)) {
      stage.addChild(animatedSprite);
    }
  }

  changeAnimation(selectedAnimation) {
    this.currentAnimation = selectedAnimation;
    for (const [animationName, animatedSprite] of Object.entries(this.animations)) {
      if (animationName === selectedAnimation) {
        animatedSprite.visible = true;
        animatedSprite.play();
      } else {
        animatedSprite.visible = false;
        animatedSprite.gotoAndStop(0);
      }
    }

    this.update();
  }

  update() {
    const [screenX, screenY] = Coordinates.gameToScreen(this.x, this.y);
    this.animations[this.currentAnimation].x = screenX;
    this.animations[this.currentAnimation].y = screenY;
    this.animations[this.currentAnimation].scale.x = this.mirrorX ? -1 : 1;
  }

  act() {
    this.update();
  }
}
