import BaseGameObject from "./BaseGameObject";
import Controls from "../Controls";
import Keys from "../Keys";
import Vec2 from 'vec2';

export default class Player extends BaseGameObject {
  constructor() {
    super({
      'idle': {
        count:5,
        frameKey: 'sprite_idle_f__FRAME__.png',
        speed: 1000
      },
      'idle2': {
        count: 5,
        frameKey: 'sprite_idle2_f__FRAME__.png',
        speed: 1000
      },
      'jump': {
        count: 6,
        frameKey: 'sprite_jump_f__FRAME__.png',
        speed: 100
      },
      'mine': {
        count: 7,
        frameKey: 'sprite_mine_f__FRAME__.png',
        speed: 100
      },
      'slash': {
        count: 5,
        frameKey: 'sprite_slash_f__FRAME__.png',
        speed: 100
      },
      'walk': {
        count: 8,
        frameKey: 'sprite_walk_f__FRAME__.png',
        speed: 100
      }
    }, 'walk', 1, 10, 10);

    this.directionals = {
      x: 0,
      y: 0,
    };
    
    this.speed = 5;

    this.controls = {
      [Keys.KEY_W]: {
        down: () => {
          this.directionals.y = 1;
        },
        up: () => {
          this.directionals.y = 0;
        }
      },
      [Keys.KEY_S]: {
        down: () => {
          this.directionals.y = -1
        },
        up: () => {
          this.directionals.y = 0;
        }
      },
      [Keys.KEY_A]: {
        down: () => {
          this.directionals.x = -1
          this.mirrorX = true;
        },
        up: () => {
          this.directionals.x = 0;
        }
      },
      [Keys.KEY_D]: {
        down: () => {
          this.directionals.x = 1
          this.mirrorX = false;
        },
        up: () => {
          this.directionals.x = 0;
        }
      },
       [Keys.KEY_F]: {
        down: () => {
          this.action = 'slash';
          this.changeAnimation('slash');
          if (this.actionTimeout) {
            clearTimeout(this.actionTimeout);
          }

          this.actionTimeout = setTimeout(() => {
            this.action = null;
          }, 250);
        
        },
        up: () => {}
      },
    };
  }

  registerControls() {
    for (const [key, action] of Object.entries(this.controls)) {
      Controls.registerObserver(key, action);
    }
  }

  act() {
    const directionalVector = new Vec2(this.directionals.x, this.directionals.y).normalize(true);

    if (!this.action) {
      if (directionalVector.length() == 0) {
        this.changeAnimation('idle');
      } else {
        this.changeAnimation('walk');
      }
    }

    this.x += directionalVector.x * this.speed;
    this.y += directionalVector.y * this.speed;

    this.update();
  }
}
