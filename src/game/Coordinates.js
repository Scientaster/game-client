import AppSingleton from "./AppSingleton";

// As a shortcut, one coord = one pixel. 
const height = 600;
const width = 800;

const SCALING = 4;

export default {
  SCALING,
  gameToScreen(gameX, gameY) {
    const {x, y} = AppSingleton.getPlayer() || {x: 0, y: 0};

    return [(gameX - x + width / 2) / SCALING, (-1 * (gameY - y) + height / 2) / SCALING];
  }
}