export default {
  ARROW_LEFT: 'ArrowLeft',
  ARROW_UP: 'ArrowUp',
  ARROW_RIGHT: 'ArrowRight',
  ARROW_DOWN: 'ArrowDown',
  KEY_W: 'KeyW',
  KEY_A: 'KeyA',
  KEY_S: 'KeyS',
  KEY_D: 'KeyD',
  KEY_F: 'KeyF',
}