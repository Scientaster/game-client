import * as PIXI from 'pixi.js';

import AppSingleton from './game/AppSingleton';
import Player from './game/obj/Player';
import GameLoader from './GameLoader';
import Controls from './game/Controls';
import Coordinates from './game/Coordinates';
import TmxMap from './tmx/TmxMap';

const app = new PIXI.Application();
// Disable interpolation when scaling, will make texture be pixelated
PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

AppSingleton.setInstance(app);
Controls.registerControls();

// The application will create a canvas element for you that you
// can then insert into the DOM.
document.body.appendChild(app.view);
app.stage.scale.set(Coordinates.SCALING);

GameLoader.loadAllAssets(() => {
  let map = new TmxMap('BaseMap');
  app.stage.addChild(map);
  let player = new Player();
  player.registerControls();
  AppSingleton.setPlayer(player);

  player.addToStage(app.stage);

  setInterval(() => {
      map.act();
      player.act();
  }, 10);
});
